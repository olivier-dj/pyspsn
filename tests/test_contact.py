"""Tests for Contact."""

from pyspsn.contact import Contact
from pyspsn.simplemanager import simple_slots


class DryRunTest:  # pylint: disable=too-few-public-methods
    """Basic dry run class."""

    __slots__ = simple_slots


def test_contact():
    """Basic tests on the contacts."""

    contact = Contact(1, 2, 10, 30, 1, 0.9, 3)
    assert contact.export() == "a contact +10 +30 1 2 1 3"
    assert str(contact) == "1->2(10-30,d3) [(10, 30)]"

    interval = contact.manager.segments[0]

    # selection of the interval
    dry_run = DryRunTest()
    contact.manager.dry_run(0, 20, None, dry_run)

    assert (
        interval
        == contact.manager.segments[dry_run.seg_index]  # pylint: disable=no-member
    )
    assert dry_run.tx_end + contact.owlt == 33  # pylint: disable=no-member

    # selection failure due to capacity
    assert not contact.manager.dry_run(0, 21, None, dry_run)

    # selection failure due to end time
    assert not contact.manager.dry_run(31, 0, None, dry_run)


def test_unscheduling():
    """Basic tests on the contacts."""

    contact = Contact(1, 2, 0, 100, 1, 0.9, 3)

    intervals = contact.manager.segments

    # selection of the interval
    dry_run = DryRunTest()
    contact.manager.dry_run(30, 20, 40, dry_run)
    contact.manager.schedule(20, 0, None, dry_run)
    contact.manager.unschedule(30, 5)
    assert str(intervals) == "[(0, 35.0), (50.0, 100)]"
    contact.manager.unschedule(45, 5)
    assert str(intervals) == "[(0, 35.0), (45, 100)]"
    contact.manager.unschedule(39, 2)
    assert str(intervals) == "[(0, 35.0), (39, 41.0), (45, 100)]"
    contact.manager.unschedule(37, 6)
    assert str(intervals) == "[(0, 35.0), (37, 43.0), (45, 100)]"
