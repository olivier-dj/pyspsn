"""Tests for SelfPriorityQueue."""

from pyspsn.selfpriorityqueue import SelfPriorityQueue, QueueElement


class Element(QueueElement):
    """SelfPriorityQueue compatible class."""

    def __init__(self, val):
        """Initialize the object with an int value."""
        super().__init__()
        self.val = val

    def __gt__(self, other):
        """For priority queue compatibility."""
        return self.val > other.val

    def __repr__(self):
        """For priority queue compatibility."""
        return str(self.val)


def test_selfpriorityqueue():
    """Basic tests on the priority queue."""

    # Creation of the queue
    queue = SelfPriorityQueue()
    assert queue.empty()
    assert str(queue) == "[]"

    element_1 = Element(5)
    element_2 = Element(2)
    element_3 = Element(10)

    # Insertion in empty queue
    queue.insert(element_1)
    assert not queue.empty()
    assert queue.first == element_1
    assert queue.last == element_1
    assert str(queue) == "[5]"

    # Pop
    element = queue.pop()
    assert element == element_1
    assert queue.empty()
    element = queue.pop()
    assert element is None
    print("======================================================")
    # erase first element
    print(queue, element_1.previous, element_1.next, queue.first, queue.last)
    queue.insert(element_1)
    print(queue, element_1.previous, element_1.next, queue.first, queue.last)
    queue.erase(element_1)
    print(queue, element_1.previous, element_1.next, queue.first, queue.last)
    assert queue.empty()

    # multiple insertions
    queue.insert(element_1)
    queue.insert(element_2)
    queue.insert(element_3)
    assert queue.first == element_2
    assert queue.first.next == element_1
    assert queue.last == element_3
    assert str(queue) == "[2, 5, 10]"

    # update of an element
    element_3.val = 4
    queue.update(element_3)
    assert queue.first == element_2
    assert queue.first.next == element_3
    assert queue.last == element_1
    assert str(queue) == "[2, 4, 5]"

    # erase element in the middle
    queue.erase(element_3)
    assert queue.first == element_2
    assert queue.first.next == element_1
    assert queue.last == element_1
    assert str(queue) == "[2, 5]"

    # erase last element
    queue.erase(element_1)
    assert queue.first == element_2
    assert queue.last == element_2
