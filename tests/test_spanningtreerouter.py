"""Quick test on the spanning tree router."""


from pyspsn.spanningtreerouter import SpanningTreeRouter
from pyspsn.contactplan import ContactPlan


def zero(_):
    """
    No EB.
    """
    return 0


def test_spanningtreerouter():
    """Quick test on the spanning tree router."""

    contact_plan = ContactPlan()
    contact_plan.load_envelope_network()

    router = SpanningTreeRouter(contact_plan.contacts, 1, mpt_method=0)

    # expiration of the bundle
    first_hop = None
    for ctct in router.route(0, [5], 5, 5, zero, ())[0]:
        first_hop = ctct

    assert first_hop is None
    assert 5 not in router.size_max

    # expiration of the route
    first_hop = None
    for ctct in router.route(100, [5], 5, 100, zero, ())[0]:
        first_hop = ctct
    assert first_hop is None
    assert router.size_max[5] == 5

    # bundle too big
    first_hop = None
    for ctct in router.route(100, [5], 20, 100, zero, ())[0]:
        first_hop = ctct
    assert first_hop is None
    assert router.size_max[5] == 5

    # destination unknown
    first_hop = None
    for ctct in router.route(0, [99], 20, 100, zero, ())[0]:
        first_hop = ctct
    assert first_hop is None


def test_multicast():
    """Quick test on the spanning tree router."""

    contact_plan = ContactPlan()
    contact_plan.load_envelope_network()

    router = SpanningTreeRouter(contact_plan.contacts, 1, mpt_method=0)

    first_hops = router.route(0, [1, 2, 3, 4, 5, 7, 6], 5, 100, zero, ())[0]

    assert len(first_hops) == 3
    for contact, destinations in first_hops.items():
        if contact.recv == 2:
            assert destinations == [2]
        if contact.recv == 3:
            assert destinations == [3, 4, 5]
        if contact.recv == 6:
            assert destinations == [6]


if __name__ == "__main__":
    test_multicast()
