"""Quick test on a random contact plan."""

import time
import argparse
from pyspsn.spsn import Spsn
from pyspsn.contactplan import ContactPlan


def test_random(max_contacts=100, max_nodes=10):
    """Quick test and demo on the envelope network."""
    contact_plan = ContactPlan()

    print("Contact plan creation")
    cp_start = time.process_time()
    contact_plan.load_random_sabr(max_contacts, max_nodes)
    cp_end = time.process_time()

    print("Multigraph creation")
    mg_start = time.process_time()
    spsn = Spsn(contact_plan.contacts)
    mg_end = time.process_time()

    print("Computation")
    spsn_start = time.process_time()
    spanning_tree = spsn.compute(1, 0, 0, ())
    spsn_end = time.process_time()
    print(spanning_tree)

    print("Contact plan creation in seconds: " + str(cp_end - cp_start))
    print("Multigraph creation in seconds: " + str(mg_end - mg_start))
    print("Spsn computation in seconds: " + str(spsn_end - spsn_start))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("max_contacts", type=int)
    parser.add_argument("max_nodes", type=int)
    args = parser.parse_args()
    test_random(args.max_contacts, args.max_nodes)
