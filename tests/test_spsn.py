"""Quick test and demo on the envelope network."""

from pyspsn.spsn import Spsn
from pyspsn.contactplan import ContactPlan


def test_spsn():
    """Test on spsn."""
    contact_plan = ContactPlan()
    contact_plan.load_envelope_network()

    spsn = Spsn(contact_plan.contacts)
    assert 2 in spsn.multigraph[1]
    assert 3 in spsn.multigraph[1]

    spsn.compute(1, 20, 0, ())
    assert 2 not in spsn.multigraph[1]
    assert 3 not in spsn.multigraph[1]

    spsn.compute(1, 110, 0, ())
    assert 1 not in spsn.multigraph

    # To make sure we have new objects
    contact_plan.load_envelope_network()
    spsn.update(contact_plan.contacts)
    assert 2 in spsn.multigraph[1]
    assert 3 in spsn.multigraph[1]
