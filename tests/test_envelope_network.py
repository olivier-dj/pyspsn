"""Quick test and demo on the envelope network."""

from pyspsn.spanningtreerouter import SpanningTreeRouter
from pyspsn.contactplan import ContactPlan


def zero(_):
    """
    No EB.
    """
    return 0


def test_envelope_network():
    """Quick test and demo on the envelope network."""

    contact_plan = ContactPlan()
    contact_plan.load_envelope_network()

    print(contact_plan.export())

    router = SpanningTreeRouter(contact_plan.contacts, 1, mpt_method=0)
    spanning_tree = router.spsn.compute(1, 0, 0, ())
    print(spanning_tree)
    print("Route to node 5 at initialization:")
    print(spanning_tree[5])

    first_hop = None
    for ctct in router.route(0, [5], 5, 100, zero, ())[0]:
        first_hop = ctct
    assert first_hop.recv in (2, 3)
    print("Scheduling a bundle of size 5 to node 5, first hop: " + str(first_hop))
    print("Route to node 5 after Scheduling:")
    print(router.routes[5])

    first_hop = None
    for ctct in router.route(0, [5], 5, 100, zero, ())[0]:
        first_hop = ctct
    assert first_hop.recv == 6
    print(
        "Scheduling a second bundle of size 5 to node 5, first hop: " + str(first_hop)
    )
    print("Route to node 5 after computation:")
    print(router.routes[5])


if __name__ == "__main__":
    test_envelope_network()
