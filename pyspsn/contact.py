"""Module defining contacts and intervals."""

from typing import Union

from pyspsn.selfpriorityqueue import QueueElement
from pyspsn.simplemanager import SimpleManager
from pyspsn.complexmanager import ComplexManager


class Contact(QueueElement):  # pylint: disable=too-many-instance-attributes
    """
    A contact between two nodes.

    Attributes
    ----------
    sndr
        The sender node.
    recv
        The receiver node.
    start
        The start time.
    end
        The end time.
    rate
        The bit rate.
    confi
        The confidence. The probability of the contact to occur.
    owlt
        The delay (one way light time).
    """

    __slots__ = [
        "sndr",
        "recv",
        "start",
        "end",
        "rate",
        "confi",
        "owlt",
        "real_recv",
        "intervals",
        "manager",
    ]

    def __init__(
        self,
        sndr: Union[str, int],
        recv: Union[str, int],
        start: float,
        end: float,
        rate: int,
        confi: float = 1.0,
        owlt: int = 0,
        real_recv=None,
        manager="simple",
    ):  # pylint: disable=too-many-arguments
        """
        Initialize a contact between two nodes.

        Parameters
        ----------
        sndr
            The sender node.
        recv
            The receiver node.
        start
            The start time.
        end
            The end time.
        rate
            The bit rate.
        confi
            The confidence.
        owlt
            The delay (one way light time).
        """
        super().__init__()
        self.sndr = sndr
        self.recv = recv
        self.start = start
        self.end = end
        self.rate = rate
        self.confi = confi
        self.owlt = owlt

        # self.recv is the graph receiver, can be virtual
        # self.real_recv is the real contact receiver
        self.real_recv = recv if real_recv is None else real_recv

        if manager == "simple":
            self.manager = SimpleManager([self])
        elif manager == "complex":
            self.manager = ComplexManager([self])
        else:
            raise NotImplementedError

    def __gt__(self, other: "Contact") -> bool:
        """
        Compare self with another element using >.

        Parameters
        ----------
        other
            The element to compare with self.

        Returns
        -------
        bool
            True if the element is greater than the other.
        """
        return self.start > other.start

    def export(self) -> str:
        """
        Return an ION compatible contact string.

        Returns
        -------
        str
            A string representing this element.
        """
        return (
            f"a contact +{self.start} +{self.end} {self.sndr}"
            f" {self.recv} {self.rate} {self.owlt}"
        )

    def __repr__(self) -> str:
        """
        Return a string representing this element.

        Returns
        -------
        str
            A string representing this element.
        """
        return (
            f"{self.sndr}->{self.recv}({self.start}"
            f"-{self.end},d{self.owlt}) {self.manager}"
        )
