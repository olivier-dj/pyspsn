"""Module for contact plans."""

import re
from typing import List
from random import randint
from pyspsn.contact import Contact


class ContactPlan:
    """
    A container for various contact plan generation options.

    Attributes
    ----------
    contacts
        The list of contacts.
    """

    def __init__(self):
        """Initialize the contact list."""
        self.contacts: List[Contact] = []

    def load_from_file(self, file_name: str):
        """
        Override the contact list from an ION compatible format file.

        Parameters
        ----------
        file_name
            The contact plan file name.
        """
        self.contacts = []

        regex = re.compile(
            "^a contact\\s+\\+(\\d+)\\s+\\+(\\d+)"
            + "\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)"
        )

        with open(file_name, "r", encoding="utf-8") as file:
            for line in file.readlines():
                matches = regex.match(line)

                if matches is not None:
                    start = int(matches.group(1))
                    end = int(matches.group(2))
                    sndr = int(matches.group(3))
                    recv = int(matches.group(4))
                    rate = int(matches.group(5))
                    owlt = int(matches.group(6))
                    confidence = 1.0

                    contact = Contact(sndr, recv, start, end, rate, confidence, owlt)
                    self.contacts.append(contact)

    def load_envelope_network(self):
        """Load the envelope network."""
        self.contacts = []

        self.contacts.append(Contact(1, 2, 0, 10, 1, 1.0, 0))
        self.contacts.append(Contact(1, 3, 0, 15, 1, 1.0, 0))
        self.contacts.append(Contact(2, 4, 5, 10, 1, 1.0, 0))
        self.contacts.append(Contact(3, 4, 5, 10, 1, 1.0, 0))
        self.contacts.append(Contact(4, 5, 5, 15, 1, 1.0, 0))
        self.contacts.append(Contact(1, 6, 50, 100, 1, 1.0, 0))
        self.contacts.append(Contact(6, 5, 50, 100, 1, 1.0, 0))

    def load_random(self, max_contacts: int, max_nodes: int):
        """
        Load random contact plan.

        Parameters
        ----------
        max_contacts
            The number of contacts.
        max_nodes
            The max number of nodes.
        """
        self.contacts = []

        for _ in range(max_contacts):
            start = randint(0, 999)
            end = start + randint(1, 100)
            from_node = randint(1, max_nodes)
            to_node = randint(1, max_nodes)
            while to_node == from_node:
                to_node = randint(1, max_nodes)
            self.contacts.append(Contact(from_node, to_node, start, end, 1, 1, 1))

    def load_random_sabr(self, max_contacts, max_nodes, max_egress=-1):
        """
        Load random contact plan with no overlapping, SABR 2.3.1.

        Parameters
        ----------
        max_contacts
            The number of contacts.
        max_nodes
            The max number of nodes.
        """
        last_end_time = {}
        max_gap = 25000
        max_duration = 50000

        if max_egress < 0:
            max_egress = max_nodes

        for _ in range(max_contacts):
            from_node = randint(1, max_nodes)

            if from_node not in last_end_time:
                last_end_time[from_node] = {}

            while True:
                to_node = randint(1, max_nodes)
                if to_node == from_node:
                    continue
                if len(last_end_time[from_node]) < max_egress:
                    break
                if to_node in last_end_time[from_node]:
                    break

            if to_node not in last_end_time[from_node]:
                last_end_time[from_node][to_node] = randint(0, max_gap)

            gap = randint(1, max_gap)
            duration = randint(1, max_duration)

            start = last_end_time[from_node][to_node] + gap
            end = start + duration
            last_end_time[from_node][to_node] = end

            self.contacts.append(Contact(from_node, to_node, start, end, 1, 1, 1))

        return last_end_time

    def get_copy(self) -> List[Contact]:
        """
        Get a copy of the contact plan.

        This method is usefull as long as SPSN alter its contacts.

        Returns
        -------
        List[Contact]
            A copy of the current contact list held.
        """
        copy_list = []
        for contact in self.contacts:
            copy_list.append(
                Contact(
                    contact.sndr,
                    contact.recv,
                    contact.start,
                    contact.end,
                    contact.rate,
                    contact.confi,
                    contact.owlt,
                )
            )

        return copy_list

    def export(self):
        """
        Return an ION compatible contact plan.

        Returns
        -------
        str
            A string representing this contact plan.
        """
        res = []
        for contact in self.contacts:
            res.append(contact.export())

        return "\n".join(res)
