"""Abtract segmentation manager to attach to each contact."""

# Minimum slots to support for the dry run object
# Extension of this list can trigger "no-member" warning from pylint
minimum_slots = ["seg_end", "tx_start", "tx_end"]


class AbstractManager:
    """Top level abstract level."""

    def __init__(self, contact_list):
        """Init from contiguous contact list."""
        raise NotImplementedError

    def schedule(self, bdl_volume, bdl_priority, bdl_ref, dry_run):
        """Manage the intervals upon scheduling."""
        # Shall return a ScheduleOutput
        raise NotImplementedError

    def dry_run(
        self, curr_time, bdl_volume, bdl_priority, dry_run, force=False
    ):  # pylint: disable=too-many-arguments
        """Check if bundles can get through this contact, store a trace."""
        raise NotImplementedError

    def unschedule(self, curr_time, bdl_volume):
        """Free bdl_volume from tx_start."""
        raise NotImplementedError


class ScheduleOutput:  # pylint: disable=too-few-public-methods
    """Shall be returned by AbstractManager.schedule(..)."""

    __slots__ = ["tx_start", "tx_end", "bundles_to_reassign"]

    def __init__(self, tx_start, tx_end, bundles_to_reassign):
        """Init."""
        self.tx_start = tx_start
        self.tx_end = tx_end
        self.bundles_to_reassign = bundles_to_reassign
