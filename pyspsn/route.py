"""Module defining routes in a spanning tree. A route shall depend of another route."""
import sys
from typing import Dict, Optional, Union

from pyspsn.contact import Contact
from pyspsn.selfpriorityqueue import QueueElement
from pyspsn.abstractmanager import minimum_slots
from pyspsn.simplemanager import simple_slots
from pyspsn.complexmanager import complex_slots


class Route(QueueElement):  # pylint: disable=too-many-instance-attributes
    """
    Class to represent a recursive route in the spanning tree.

    Attributes
    ----------
    to_node
        The nodes reached via this route.
    via_contact: Optional[Contact]
        The contact to reach this node.
    via_interval: Optional[Interval]
        The interval to reach this node.
    parent: Optional[Route]
        The direct parent to this route.
    parenting_set
        Set to True if parenting (dict children) has been initialized.
    children: Dict[Union[str, int], Route]
        The children of this route, not initialized by default.
    arrival_time: float
        Arrival time at the node.
    expiration: float
        Expiration of the route (informational).
    hop_count
        Hop count (informational).
    confidence
        Confidence of the route.
    sumowlt
        Cumulative delay over the previous hop.
    """

    __slots__ = (
        [
            "to_node",
            "via_contact",
            "parenting_set",
            "children",
            "parent",
            "arrival_time",
            "expiration",
            "hop_count",
            "confidence",
            "sumowlt",
        ]
        + minimum_slots
        + complex_slots
        + simple_slots
    )

    def __init__(
        self,
        to_node: Union[str, int],
        via_contact: Contact = None,
    ):
        """
        Initialize a route to a given node via a given contact and interval.

        Parameters
        ----------
        to_node
            The nodes reached via this route.
        via_contact: Optional[Contact]
            The contact to reach this node.
        via_interval: Optional[Interval]
            The interval to reach this node.
        """
        super().__init__()
        self.to_node = to_node
        self.via_contact: Optional[Contact] = via_contact

        # Spanning Tree
        self.parenting_set = False
        self.children: Optional[Dict[Union[str, int], Route]] = None

        # route metrics
        self.parent: Optional[Route] = None
        self.arrival_time: float = sys.maxsize
        self.expiration: float = sys.maxsize
        self.hop_count = 0
        self.confidence = 1.0
        self.sumowlt = 0

        self.seg_end = None
        self.tx_start = None
        # complex
        self.tx_end = None
        self.left_aggr = None
        self.left_aggr_first_seg = None
        self.volume_left = None
        self.middle_aggr_first_seg = None
        self.right_aggr = None
        self.right_aggr_first_seg = None
        self.volume_right = None
        # simple
        self.seg_index = None
        self.split = None

    def copy_to_target(self, target):
        """If the route shall replace a target, update the target."""
        # route
        target.to_node = self.to_node
        target.via_contact = self.via_contact
        target.parenting_set = self.parenting_set
        target.children = self.children
        target.parent = self.parent
        target.arrival_time = self.arrival_time
        target.expiration = self.expiration
        target.hop_count = self.hop_count
        target.confidence = self.confidence
        target.sumowlt = self.sumowlt
        # abstract
        target.seg_end = self.seg_end
        target.tx_start = self.tx_start
        # complex
        target.tx_end = self.tx_end
        target.left_aggr = self.left_aggr
        target.left_aggr_first_seg = self.left_aggr_first_seg
        target.volume_left = self.volume_left
        target.middle_aggr_first_seg = self.middle_aggr_first_seg
        target.right_aggr = self.right_aggr
        target.right_aggr_first_seg = self.right_aggr_first_seg
        target.volume_right = self.volume_right
        # simple
        target.seg_index = self.seg_index
        target.split = self.split

    def __gt__(self, other):
        """
        Compare self with another element using >.

        Parameters
        ----------
        other
            The element to compare with self.

        Returns
        -------
        bool
            True if the element is greater than the other.
        """
        if self.arrival_time > other.arrival_time:
            return True
        if self.arrival_time == other.arrival_time:
            if self.hop_count > other.hop_count:
                return True
            if self.hop_count == other.hop_count:
                if self.expiration < other.expiration:
                    return True

        return False

    def __repr__(self):
        """
        Return a string representing this element.

        Returns
        -------
        str
            A string representing this element.
        """
        routes = []
        hops = []
        curr = self

        # This is in practice unecessary, just there to check viability of the route
        while curr.parent is not None:
            hops.insert(0, curr.via_contact)
            routes.insert(0, curr)
            curr = curr.parent

        if len(routes) == 0:
            return f"No route to {self.to_node} (Source?)\n"

        first_hop = routes[0]

        hops_list = []

        for hop in hops:
            hops_list.append("\t" + str(hop))

        hops_string = "\n".join(hops_list)
        max_of = max(first_hop.parent.arrival_time, first_hop.via_contact.start)
        return (
            f"to:{self.to_node}|via:{first_hop.to_node}("
            f"{float(max_of):03}"
            f",{float(self.expiration):03})|bdt:{self.arrival_time}"
            f"|hops:{self.hop_count}|conf:"
            f"{self.confidence}|\n{hops_string}\n"
        )
