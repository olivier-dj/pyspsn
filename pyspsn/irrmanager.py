"""Module for interregional routing support."""
import json

from pyspsn.selfpriorityqueue import SelfPriorityQueue, QueueElement


class ForwardingTableEntry(QueueElement):
    """A Forwarding table entry."""

    __slots__ = ["region", "first_hop", "hop_count"]

    def __init__(self, region, first_hop, hop_count):
        """Init."""
        super().__init__()
        self.region = region
        self.first_hop = first_hop
        self.hop_count = hop_count

    def __gt__(self, other):
        """For the SelfPriorityQueue."""
        return self.hop_count > other.hop_count

    def __repr__(self):
        """To print the object."""
        return f"To region {self.region}: {self.hop_count} hops via {self.first_hop}"


class IRRManager:
    """A manager for IRR pathfinding."""

    __slots__ = [
        "forwarding_table",
        "root_region",
        "neighboring",
        "membership",
        "region_for_node",
        "home_region",
    ]

    def __init__(self, config_file_path, local_node, use_shortcuts):
        """Init."""
        with open(config_file_path, "r", encoding="utf-8") as config:
            config_dict = json.load(config)

        self.forwarding_table = {}
        self.root_region = config_dict["root_region"]
        self.neighboring = {int(k): v for k, v in config_dict["neighboring"].items()}
        self.membership = {int(k): v for k, v in config_dict["membership"].items()}

        # For per-node quick access, this structure is preferable
        self.region_for_node = {}
        for region, nodes in self.membership.items():
            for node in nodes:
                self.region_for_node[node] = region

        if use_shortcuts:
            for shortcut in config_dict["shortcuts"]:
                self.neighboring[shortcut[0]].append(shortcut[1])

        self.home_region = self.region_for_node[local_node]
        self._update_table()

    def is_from_home_region(self, node):
        """Check if a node is from home region."""
        return self.region_for_node[node] == self.home_region

    def convert(self, node):
        """Return converted name (if applicable) of a node."""
        # The node is part of the region, do not translate
        if self.region_for_node[node] == self.home_region:
            return node
        return self.region_for_node[node]

    def get_irr_destination(self, destination):
        """Return the first hop."""
        # unknown node
        if destination not in self.region_for_node:
            return None

        target_region = self.region_for_node[destination]

        # If the node is within the region, we do not need translation
        if target_region == self.home_region:
            return destination

        # Path unavailable
        if target_region not in self.forwarding_table:
            return None

        return self.forwarding_table[target_region].first_hop

    def update_neighboring_of_region(self, region, neighbors):
        """Update the neighboring for a region."""
        self.neighboring[region] = neighbors
        self._update_table()

    def _update_table(self):
        """Compute the forwarding table."""
        self.forwarding_table = {}
        source_route = ForwardingTableEntry(self.home_region, None, 0)
        self.forwarding_table[self.home_region] = source_route
        acc = SelfPriorityQueue()
        acc.insert(source_route)

        while not acc.empty():
            sndr_entry = acc.pop()
            sndr_region = sndr_entry.region
            # Expected to happen only if unidirectional links exist
            if sndr_region not in self.neighboring:
                continue

            hop_count = sndr_entry.hop_count + 1
            for recv_region in self.neighboring[sndr_region]:
                if sndr_region == self.home_region:
                    first_hop = recv_region
                else:
                    first_hop = sndr_entry.first_hop

                if recv_region not in self.forwarding_table:
                    recv_entry = ForwardingTableEntry(recv_region, first_hop, hop_count)
                    self.forwarding_table[recv_region] = recv_entry
                    acc.update(recv_entry)
                else:
                    recv_entry = self.forwarding_table[recv_region]
                    if hop_count < recv_entry.hop_count:
                        recv_entry.hop_count = hop_count
                        recv_entry.first_hop = first_hop
                        acc.update(recv_entry)
