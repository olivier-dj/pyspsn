"""
Contact segmentation manager to attach to each contact.

Module to manage on a contact basis the volume with class of service
and data rate variation support
"""
from pyspsn.abstractmanager import (
    AbstractManager,
    ScheduleOutput,
    minimum_slots,
)

complex_slots = [
    "left_aggr",
    "left_aggr_first_seg",
    "volume_left",
    "middle_aggr_first_seg",
    "right_aggr",
    "right_aggr_first_seg",
    "volume_right",
] + minimum_slots


class ComplexManagerDryRun:  # pylint: disable=too-few-public-methods,
    # pylint: disable=too-many-instance-attributes
    """Dry run metrics."""

    __slots__ = complex_slots

    def __init__(self):  # pylint: disable=too-many-arguments
        """Init."""


# This is used in the scheduled functions
DRY_RUN_SINGLETON = ComplexManagerDryRun()


class LkList:
    """Double linked list implementation."""

    __slots__ = ["first", "last"]

    def __init__(self):
        """Init."""
        self.first = None
        self.last = None

    def insert_after(self, element, new_element):
        """Insert after a given element, at first position if None."""
        if element is None:

            if self.first is None:
                succ_of_new = None
            else:
                succ_of_new = self.first
                succ_of_new.predecessor = new_element

            new_element.predecessor = None
            new_element.successor = succ_of_new
            self.first = new_element
        else:
            if element.successor is not None:
                element.successor.predecessor = new_element

            new_element.predecessor = element
            new_element.successor = element.successor
            element.successor = new_element

        if new_element.successor is None:
            self.last = new_element

    def remove_range(self, from_obj, to_obj):  # , acc=None):
        """Remove all elements between to elements."""
        if to_obj is self.last:
            self.last = from_obj.predecessor

        curr = from_obj
        # last_inserted = None

        while curr is not None:
            if curr is to_obj:
                after = None
            else:
                after = curr.successor

            self.remove(curr)
            # if acc is not None:
            #     acc.insert_after(last_inserted, curr)
            #     last_inserted = curr

            curr = after

    def extend(self, other):
        """Add in self the elements from other."""
        # self is empty
        if self.first is None:
            self.first = other.first
            self.last = other.last

        # other is empty
        elif other.first is None:
            pass
        # both are not empty
        else:
            self.last.successor = other.first
            other.first.predecessor = self.last
            self.last = other.last

    def remove(self, element):
        """Remove element for the list."""
        if element is self.last:
            self.last = element.predecessor

        old_predecessor = element.predecessor
        old_successor = element.successor

        if old_predecessor is None:
            self.first = old_successor
        else:
            old_predecessor.successor = old_successor

        if old_successor is None:
            self.last = old_predecessor
        else:
            old_successor.predecessor = old_predecessor

        element.successor = None
        element.predecessor = None

    def __repr__(self):
        """To print the object."""
        strg = ""
        curr = self.first
        if curr is None:
            return "\t\t\tNone"
        while True:
            strg += str(curr)
            if curr == self.last:
                break
            curr = curr.successor

        return strg


class Interval:  # pylint: disable=too-few-public-methods
    """Top level abstract level."""

    __slots__ = ["start", "end"]

    def __init__(self, start, end):
        """Init."""
        self.start = start
        self.end = end


class LinkedInterval(Interval):  # pylint: disable=too-few-public-methods
    """An interval that can be inserted in a LkList."""

    __slots__ = ["predecessor", "successor"]

    def __init__(self, start, end):
        """Init."""
        super().__init__(start, end)
        self.predecessor = None
        self.successor = None


class Segment(Interval):  # pylint: disable=too-few-public-methods
    """An interval with a data rate assumed constant."""

    __slots__ = ["data_rate"]

    def __init__(self, start, end, data_rate):
        """Init."""
        super().__init__(start, end)
        self.data_rate = data_rate

    def __repr__(self):
        """To print the object."""
        return (
            f"\t\tSegment from {self.start} to {self.end}, for rate {self.data_rate}\n"
        )


class BundleBooking(LinkedInterval):  # pylint: disable=too-few-public-methods
    """An interval booked for a bundle."""

    __slots__ = ["bundle_ref"]

    def __init__(self, start, end, bundle_ref):
        """Init."""
        super().__init__(start, end)
        self.bundle_ref = bundle_ref

    def __repr__(self):
        """To print the object."""
        return f"\t\t\tBundle {self.bundle_ref} from {self.start} to {self.end}\n"


class Aggregate(LinkedInterval):  # pylint: disable=too-few-public-methods
    """An interval booked for bundles of same priority."""

    __slots__ = ["first_segment_index", "priority", "volume", "bundle_list"]

    def __init__(
        self, start, end, first_segment_index, priority, volume
    ):  # pylint: disable=too-many-arguments
        """Init."""
        super().__init__(start, end)
        self.first_segment_index = first_segment_index
        self.priority = priority
        self.volume = volume
        self.bundle_list = LkList()

    def __repr__(self):
        """To print the object."""
        return (
            f"\t\tAggregate with segments from index {self.first_segment_index}, "
            f"from {self.start} to {self.end}, vol of {self.volume} "
            f"and prio {self.priority} "
            f"bundles are:\n{self.bundle_list} \n"
        )


class ComplexManager(AbstractManager):  # pylint: disable=too-many-instance-attributes
    """To level object to manage the volume."""

    __slots__ = ["segment_list", "aggregate_list", "start", "end"]

    def __init__(self, contact_list):
        """Init."""
        curr_data_rate = -1
        curr_end = contact_list[0].start

        volume = 0
        first_seg = None

        # Non-mutable
        self.segment_list = []
        # Mutable
        self.aggregate_list = LkList()

        self.start = curr_end

        # Verify contiguity and data rate variable, create segments
        for curr_contact in contact_list:
            curr_start = curr_contact.start

            # curr end and data_rate are still from the predecessor contact
            # assert curr_data_rate != curr_contact.rate
            # assert curr_end == curr_contact.start

            curr_data_rate = curr_contact.rate
            curr_end = curr_contact.end
            volume += (curr_end - curr_start) * curr_data_rate

            seg = Segment(curr_start, curr_end, curr_data_rate)

            # if the first segment
            if first_seg is None:
                first_seg = seg

            self.end = seg.end
            self.segment_list.append(seg)

        self.aggregate_list.insert_after(
            None, Aggregate(first_seg.start, seg.end, 0, -1, volume)
        )

    def __repr__(self):
        """To print the object."""
        string = f"\tContact from {self.start} to {self.end} with those Aggregates:\n"
        string += str(self.aggregate_list)
        string += "\tSegments:\n"
        for seg in self.segment_list:
            string += str(seg)
        return string

    def _get_volume(self, aggr, tx_start, tx_end):
        """Get the residual available volume between tx_start and tx_end."""
        volume = 0
        curr_end = -1
        seg_index = aggr.first_segment_index
        curr_time = tx_start

        while curr_time < tx_end:
            seg = self.segment_list[seg_index]
            if seg.end <= tx_start:
                seg_index += 1
                continue

            curr_end = seg.end if tx_end > seg.end else tx_end
            curr_time = tx_start if tx_start > seg.start else seg.start

            volume += (curr_end - curr_time) * seg.data_rate

            if curr_end == tx_end:
                return volume

            seg_index += 1

        return 0

    def _try_on_aggregate(self, aggr, curr_time, remaining_bytes):
        """Check if transmission is possible from this aggregate (and the next ones)."""
        first_index = -1
        seg_index = aggr.first_segment_index
        count = len(self.segment_list)

        # for each segment, take the volume
        while seg_index < count:
            seg = self.segment_list[seg_index]

            # We reach the next aggregate, stop
            if seg.start >= aggr.end or curr_time >= aggr.end:
                break

            if curr_time >= seg.end:
                seg_index += 1
                continue

            if first_index < 0:
                first_index = seg_index

            # curr_time becomes tx_end
            tx_start = curr_time if curr_time >= aggr.start else aggr.start
            tx_end = curr_time + remaining_bytes / seg.data_rate

            limit = min(aggr.end, seg.end)

            if tx_end == limit:
                return tx_end, 0, first_index, seg_index + 1
            if tx_end < limit:
                return tx_end, 0, first_index, seg_index

            remaining_bytes -= (limit - tx_start) * seg.data_rate
            curr_time = limit

            seg_index += 1

        return curr_time, remaining_bytes, first_index, seg_index

    def _try_merge(self, first_aggr, second_aggr):
        """Merge 2 aggregates."""
        if first_aggr is None:
            return second_aggr
        if second_aggr is None:
            return first_aggr

        if first_aggr.priority == second_aggr.priority:
            new_end = second_aggr.end
            volume = second_aggr.volume
            first_aggr.bundle_list.extend(second_aggr.bundle_list)
            self.aggregate_list.remove(second_aggr)
            first_aggr.end = new_end
            first_aggr.volume += volume
            return first_aggr

        return second_aggr

    def _get_bundles_to_reassign(self, left_aggr, right_aggr):
        """Return a list of all bundles within 2 non-contiguous aggregates."""
        bundles = LkList()

        curr = left_aggr

        while curr is not None:
            bundles.extend(curr.bundle_list)

            if curr is right_aggr:
                curr = None
            else:
                curr = curr.successor

        return bundles

    def _reassign(self, start, end, source, target):
        """Reassign bundles from the source to the target."""
        curr_bdl = source.first

        while curr_bdl is not None:
            if curr_bdl.start >= end:
                break

            next_bdl = curr_bdl.successor
            if curr_bdl.start >= start and curr_bdl.end <= end:
                source.remove(curr_bdl)
                target.insert_after(target.last, curr_bdl)

            curr_bdl = next_bdl

    def unschedule(self, curr_time, bdl_volume):
        """Revert changes to apply priority -1."""
        if self.dry_run(curr_time, bdl_volume, -1, DRY_RUN_SINGLETON, force=True):
            self.schedule(bdl_volume, -1, None, DRY_RUN_SINGLETON)

    def _bundle_lklist_to_list(self, lkl):
        """Convert for python convenience."""
        output = []
        curr = lkl.first
        while curr is not None:
            output.append(curr.bundle_ref)
            curr = curr.successor

        return output

    def schedule(
        self, bdl_volume, bdl_priority, bdl_ref, dry_run
    ):  # pylint: disable=too-many-locals
        """Manage the intervals upon scheduling."""
        bound_left = dry_run.left_aggr.start
        bound_right = dry_run.right_aggr.end
        left_prio = dry_run.left_aggr.priority
        right_prio = dry_run.right_aggr.priority

        append_after = dry_run.left_aggr.predecessor

        bundles_to_reassign = self._get_bundles_to_reassign(
            dry_run.left_aggr, dry_run.right_aggr
        )
        self.aggregate_list.remove_range(dry_run.left_aggr, dry_run.right_aggr)

        if dry_run.volume_left > 0:
            left = Aggregate(
                bound_left,
                dry_run.tx_start,
                dry_run.left_aggr_first_seg,
                left_prio,
                dry_run.volume_left,
            )
            self._reassign(
                bound_left, dry_run.tx_start, bundles_to_reassign, left.bundle_list
            )
            self.aggregate_list.insert_after(append_after, left)
            if left.bundle_list.first is None:
                left.priority = -1
            append_after = self._try_merge(append_after, left)

        middle = Aggregate(
            dry_run.tx_start,
            dry_run.tx_end,
            dry_run.middle_aggr_first_seg,
            bdl_priority,
            bdl_volume,
        )
        if bdl_ref is not None:
            middle.bundle_list.insert_after(
                None, BundleBooking(dry_run.tx_start, dry_run.tx_end, bdl_ref)
            )
        self.aggregate_list.insert_after(append_after, middle)
        append_after = self._try_merge(append_after, middle)

        if dry_run.volume_right > 0:
            right = Aggregate(
                dry_run.tx_end,
                bound_right,
                dry_run.right_aggr_first_seg,
                right_prio,
                dry_run.volume_right,
            )
            self._reassign(
                dry_run.tx_end, bound_right, bundles_to_reassign, right.bundle_list
            )
            self.aggregate_list.insert_after(append_after, right)
            if right.bundle_list.first is None:
                right.priority = -1
            append_after = self._try_merge(append_after, right)

        self._try_merge(append_after, append_after.successor)

        # bundle that were not reassigned are between tx_start and tx_end
        # need to be rescheduled
        return ScheduleOutput(
            dry_run.tx_start,
            dry_run.tx_end,
            self._bundle_lklist_to_list(bundles_to_reassign),
        )

    def dry_run(
        self, curr_time, bdl_volume, bdl_priority, dry_run, force=False
    ):  # pylint: disable=too-many-locals, too-many-arguments, too-many-branches
        # pylint: disable=too-many-statements
        """Check if bundles can get through this contact, returns a trace."""
        dry_run.tx_end = -1
        dry_run.tx_start = -1
        dry_run.volume_left = 0
        remaining_bytes = bdl_volume
        dry_run.volume_right = 0
        dry_run.seg_end = -1
        dry_run.left_aggr = None
        dry_run.left_aggr_first_seg = -1
        dry_run.middle_aggr_first_seg = -1
        dry_run.right_aggr = None
        dry_run.right_aggr_first_seg = -1

        aggr = self.aggregate_list.first

        while aggr is not None:

            # aggregate already ended
            if curr_time >= aggr.end:
                aggr = aggr.successor
                continue

            # Now aggregates are selectable

            # Check unsuitability also if we do not force the update
            if not force:
                # This aggregate is unsuitable
                if bdl_priority <= aggr.priority:
                    # reset, we want continguous aggregates
                    dry_run.tx_start = -1
                    dry_run.left_aggr = None
                    dry_run.left_aggr_first_seg = None
                    remaining_bytes = bdl_volume
                    dry_run.volume_left = 0
                    aggr = aggr.successor
                    continue
            else:
                if bdl_priority != aggr.priority:
                    dry_run.tx_start = -1
                    dry_run.left_aggr = None
                    dry_run.left_aggr_first_seg = None
                    remaining_bytes = bdl_volume
                    dry_run.volume_left = 0
                    aggr = aggr.successor
                    continue

            if curr_time < aggr.start:
                curr_time = aggr.start

            # this is the first aggregate
            if aggr.start < curr_time:
                dry_run.volume_left = self._get_volume(aggr, aggr.start, curr_time)

            if dry_run.tx_start < 0:
                dry_run.tx_start = curr_time

            (
                curr_time,
                remaining_bytes,
                first_seg,
                first_after_tx_seg,
            ) = self._try_on_aggregate(aggr, curr_time, remaining_bytes)

            dry_run.right_aggr = aggr
            dry_run.right_aggr_first_seg = first_after_tx_seg

            if dry_run.left_aggr is None:
                dry_run.left_aggr = aggr
                dry_run.left_aggr_first_seg = aggr.first_segment_index
                dry_run.middle_aggr_first_seg = first_seg

            if remaining_bytes == 0:
                if curr_time < dry_run.right_aggr.end:
                    dry_run.volume_right = self._get_volume(
                        aggr, curr_time, dry_run.right_aggr.end
                    )

                dry_run.tx_end = curr_time
                dry_run.seg_end = aggr.end

                # This loop is used to calculate the expiration time
                while aggr is not None:
                    if bdl_priority > aggr.priority:
                        dry_run.seg_end = aggr.end
                    else:
                        break
                    aggr = aggr.successor

                return True

            aggr = aggr.successor

        return False


###########################################
# some helper classes/functions for tests #
###########################################


class Contact(Interval):  # pylint: disable=too-few-public-methods
    """A minimal contact object."""

    def __init__(self, start, end, data_rate):
        """Init."""
        super().__init__(start, end)
        self.rate = data_rate

    def __repr__(self):
        """To print the object."""
        return f"Contact from {self.start} to {self.end} for rate {self.rate}"


class Bundle:  # pylint: disable=too-few-public-methods
    """A minimal bundle object."""

    def __init__(self, size, prio, name):
        """Init."""
        self.size = size
        self.prio = prio
        self.name = name
        self.plan = []

    def __repr__(self):
        """To print the object."""
        return f"{self.name}"


def try_schedule(mng, curr_time, bdl):
    """Help for tests."""
    print(
        f" -- Scheduling bundle {bdl.name} of size/priority "
        f"{bdl.size}/{bdl.prio} at {curr_time} --"
    )

    print("STATE\n", mng)
    print("Processing dry run")
    dry_run = ComplexManagerDryRun()
    res = mng.dry_run(curr_time, bdl.size, bdl.prio, dry_run)
    if res is False:
        print("Impossible, dry run failed")
    else:
        print(
            f"Transmission planned from {dry_run.tx_start}"  # pylint: disable=no-member
            f" to {dry_run.tx_end}, processing"  # pylint: disable=no-member
        )
        res = mng.schedule(bdl.size, bdl.prio, bdl, dry_run)
        bdl.plan.append((res.tx_start, bdl.size, mng))

        if len(res.bundles_to_reassign) == 0:
            print("No bundles to reschedule\n")
        else:
            bdls = res.bundles_to_reassign
            print("\tBundle to reschedule:\n\t\t", bdls)

        print("OUTCOME\n", mng)


#################
# test examples #
#################

if __name__ == "__main__":
    cp = []

    cp.append(Contact(10, 30, 1))
    cp.append(Contact(30, 50, 2))
    cp.append(Contact(50, 80, 1))

    print("Contact plan:")
    for ctct in cp:
        print(ctct)

    print("\n---")

    manager = ComplexManager(cp)

    CURR_TIME = 10
    bundle = Bundle(20, 2, "bdl_1")

    try_schedule(manager, CURR_TIME, bundle)

    CURR_TIME = 10
    bundle = Bundle(40, 2, "bdl_2")
    try_schedule(manager, CURR_TIME, bundle)

    CURR_TIME = 10
    bundle = Bundle(40, 1, "bdl_3")

    try_schedule(manager, CURR_TIME, bundle)

    CURR_TIME = 15
    bundle = Bundle(40, 4, "bdl_4")

    try_schedule(manager, CURR_TIME, bundle)

    CURR_TIME = 30
    bundle = Bundle(15, 3, "bdl_5")

    try_schedule(manager, CURR_TIME, bundle)
    manager.unschedule(15, 55)
    print(manager)
