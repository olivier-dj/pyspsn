"""Segmentation manager with no priority or data rate support."""
from pyspsn.abstractmanager import (
    AbstractManager,
    ScheduleOutput,
    minimum_slots,
)

simple_slots = [
    "seg_index",
    "split",
] + minimum_slots


class Segment:  # pylint: disable=too-few-public-methods
    """Interval."""

    __slots__ = ["start", "end"]

    def __init__(self, start, end):
        """Init the interval."""
        self.start = start
        self.end = end

    def __repr__(self) -> str:
        """Str repr."""
        return f"({self.start}, {self.end})"


class SimpleManager(AbstractManager):
    """Top level abstract level."""

    __slots__ = ["rate", "segments"]

    def __init__(self, contact_list):
        """Init from contiguous contact list."""
        if len(contact_list) != 1:
            raise ValueError("This manager does not support contiguous contacts!")

        contact = contact_list[0]
        self.rate = contact.rate
        self.segments = [Segment(contact.start, contact.end)]

    def schedule(self, bdl_volume, bdl_priority, bdl_ref, dry_run):
        """Manage the intervals upon scheduling."""
        target = self.segments[dry_run.seg_index]

        if dry_run.split:
            self.segments.insert(
                dry_run.seg_index, Segment(target.start, dry_run.tx_start)
            )

        target.start = dry_run.tx_end

        return ScheduleOutput(dry_run.tx_start, dry_run.tx_end, [])

    def dry_run(
        self, curr_time, bdl_volume, bdl_priority, dry_run, force=False
    ):  # pylint: disable=too-many-arguments
        # pylint: disable=too-many-arguments
        """Check if bundles can get through this contact, store a trace."""
        # bdl_priority is ignored in this version
        tx_time = bdl_volume / self.rate
        for index, seg in enumerate(self.segments):
            if curr_time > seg.end:
                continue

            dry_run.tx_start = curr_time if curr_time > seg.start else seg.start
            dry_run.tx_end = dry_run.tx_start + tx_time

            if dry_run.tx_end <= seg.end:
                dry_run.seg_index = index
                dry_run.seg_end = seg.end
                dry_run.split = dry_run.tx_start != seg.start

                return True
        return False

    def unschedule(self, curr_time, bdl_volume):
        """Revert changes."""
        new_seg_start = curr_time
        new_seg_end = curr_time + bdl_volume / self.rate

        first_to_remove = -1
        last_to_remove = -1
        remove = False

        insert_index = 0

        for index, seg in enumerate(self.segments):

            max_start = seg.start
            min_start = new_seg_start
            if new_seg_start > seg.start:
                max_start = new_seg_start
                min_start = seg.start

            max_end = seg.end
            min_end = new_seg_end
            if new_seg_end > seg.end:
                max_end = new_seg_end
                min_end = seg.end

            if new_seg_start < seg.start:
                insert_index = index

            # Overlap
            if min_end >= max_start:
                remove = True
                new_seg_start = min_start
                new_seg_end = max_end
                last_to_remove = index + 1
                if first_to_remove < 0:
                    first_to_remove = index
            else:
                continue

            if new_seg_start > seg.end:
                break

        if remove:
            del self.segments[first_to_remove:last_to_remove]
            insert_index = first_to_remove

        self.segments.insert(insert_index, Segment(new_seg_start, new_seg_end))

    def __repr__(self) -> str:
        """Str repr."""
        return str(self.segments)
