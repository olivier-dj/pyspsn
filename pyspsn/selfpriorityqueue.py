"""
Module for self priority queues.

This could have been done with a heapq, but the heapq
doesn't support updates and removals of non-first elements.
"""

from typing import Optional


class QueueElement:
    """
    Element of a SelfPriorityQueue.

    Attributes
    ----------
    previous
        Previous element of the queue.
    next
        Next element of the queue.
    """

    __slots__ = ["previous", "next", "queued"]

    def __init__(self):
        """Initialize the previous and the next element."""
        self.previous = None
        self.next = None
        self.queued = False

    def __repr__(self) -> str:
        """
        Return a string representing this element.

        Returns
        -------
        str
            A string representing this element.
        """
        raise NotImplementedError

    def __gt__(self, other) -> bool:
        """
        Compare self with another element using >.

        Parameters
        ----------
        other
            The element to compare with self.

        Returns
        -------
        bool
            True if the element is greater than the other.
        """
        raise NotImplementedError


class SelfPriorityQueue:
    """
    Class to chain the elements with each other.

    This is a self priority queue because the elements
    are linked with each other without a link object.

    Attributes
    ----------
    first
        First element of the queue.
    last
        Last element of the queue.
    """

    __slots__ = ["first", "last"]

    def __init__(self):
        """Initialize the priority queue."""
        self.first = None
        self.last = None

    def insert(self, element: QueueElement):
        """
        Add an element in the queue.

        Parameters
        ----------
        element
            The new element to insert in the queue.
        """
        if element.queued:
            raise ValueError("Already in queue")
        element.queued = True

        if self.first is None:
            self.first = element
            self.last = element
            element.previous = None
            element.next = None
            return

        if self.first > element:
            self.first.previous = element
            element.next = self.first
            self.first = element
            element.previous = None
            return

        if element > self.last:
            self.last.next = element
            element.previous = self.last
            self.last = element
            element.next = None
            return

        new_previous = self.last.previous
        new_next = self.last

        while True:
            if new_previous is None:
                self.first.previous = element
                element.next = self.first
                self.first = element
                element.previous = None
                return

            if element > new_previous or new_next > element:
                new_previous.next = element
                new_next.previous = element
                element.previous = new_previous
                element.next = new_next
                return

            new_next = new_next.previous
            new_previous = new_previous.previous

    def pop(self) -> Optional[QueueElement]:
        """
        Remove and return the first element.

        Returns
        -------
        QueueElement
            The first element of the queue when there is one.
        """
        if self.first is None:
            return None

        element = self.first

        if element.next is None:
            self.last = None
            self.first = None
        else:
            self.first = element.next
            self.first.previous = None

        element.queued = False

        return element

    def empty(self) -> bool:
        """
        Check if the queue is empty.

        Returns
        -------
        bool
            True if the queue is empty.
        """
        return self.first is None

    def erase(self, element: QueueElement):
        """
        Erase the element from the queue.

        Parameters
        ----------
        element
            Element to remove if it is part of the queue.
        """
        if not element.queued:
            # we can casually try to remove from the acc,
            # even if the route was already treated
            return

        old_previous = element.previous
        old_next = element.next

        if old_previous is None:
            self.first = old_next
        else:
            old_previous.next = old_next

        if old_next is None:
            self.last = old_previous
        else:
            old_next.previous = old_previous

        element.queued = False

    def update(self, element: QueueElement):
        """
        Update the positon of an element or insert it in the queue.

        Parameters
        ----------
        element
            The element to insert or update in the queue.
        """
        if not element.queued:
            self.insert(element)
        else:
            self.erase(element)
            self.insert(element)

    def __repr__(self) -> str:
        """
        Allow to print a queue.

        Returns
        -------
        str
            Representation of the queue.
        """
        element = self.first
        result = "["
        while element is not None:
            result += str(element)
            element = element.next
            if element is not None:
                result += ", "

        return result + "]"
