"""Module for spanning trees."""

from typing import List, Union, Optional, Dict
from pyspsn.spsn import Spsn
from pyspsn.contact import Contact
from pyspsn.route import Route


class CacheEntry:  # pylint: disable=too-few-public-methods
    """A cache entry, WIP."""

    __slots__ = ["tree", "shadow_level", "exclusions"]

    def __init__(self, tree, shadow_level, exclusions):
        """WIP."""
        assert isinstance(exclusions, tuple)
        self.tree = tree
        self.shadow_level = shadow_level
        self.exclusions = exclusions


class Cache:
    """A cache, WIP."""

    __slots__ = ["max_entries", "order", "entries"]

    def __init__(self, max_entries):
        """WIP."""
        # if max_entries is 0, no caching occurs
        self.max_entries = max_entries
        self.order = []
        self.entries = {}

    def load(self, size, exclusions):
        """Return the best entry, WIP."""
        if len(self.entries) == 0 or exclusions not in self.entries:
            return None

        candidate = self.entries[exclusions]

        if candidate.shadow_level <= size:
            return candidate
        return None

    def store(self, new_entry):
        """Store a new entry, WIP."""
        # If the key exists, replace and exit
        if self.max_entries == 0:
            return

        exclusions = new_entry.exclusions
        if exclusions in self.entries:
            self.entries[exclusions] = new_entry
            return

        # If the cache is full, make room by removing the first inserted key
        if len(self.order) == self.max_entries:
            key = self.order.pop(0)
            del self.entries[key]

        # Insert
        self.order.append(exclusions)
        self.entries[exclusions] = new_entry


class SpanningTreeRouter:  # pylint: disable=too-many-instance-attributes
    """A router using a spanning tree."""

    __slots__ = ["spanning_tree", "spsn", "routes", "source_node", "size_max", "cache"]

    def __init__(  # pylint: disable=too-many-arguments
        self,
        contacts: List[Contact],
        source_node: Union[str, int],
        managed=True,
        mpt_method=1,
        cache_size=10,
    ):
        """Initialize the spanning tree for a given source."""
        self.spsn = Spsn(
            contacts,
            managed=managed,
            mpt_method=mpt_method,
        )
        self.routes: Dict[Union[str, int], Route] = {}
        self.source_node = source_node
        self.size_max: Dict[Union[str, int], int] = {}
        self.cache = Cache(cache_size)

    def route(  # pylint: disable=too-many-arguments,
        # pylint: disable=too-many-return-statements, too-many-branches
        self,
        curr_time: float,
        destinations: List[Union[str, int]],
        size: int,
        expiration: float,
        eb_cost_func,
        exclusions,
        priority=0,
        bundle_ref=None,
    ) -> Optional[Contact]:
        """Return the first hops dict for the bundle to be routed."""
        assert isinstance(exclusions, tuple)
        unreachable_count = 0
        for dest in destinations:
            if dest in self.size_max:
                if self.size_max[dest] <= size:
                    # We already failed to schedule a smaller bundle
                    unreachable_count += 1

        if unreachable_count == len(destinations):
            return {}, None, None

        entry = self.cache.load(size, exclusions)

        if entry is not None:
            self.routes = entry.tree
            success, booking, bundles_to_reschedule = self._schedule_multicast(
                curr_time,
                destinations,
                size,
                expiration,
                eb_cost_func,
                True,
                priority,
                bundle_ref,
            )
            if success:
                return (
                    self._first_hop_contact(destinations),
                    booking,
                    bundles_to_reschedule,
                )

        self.routes = self.spsn.compute(
            self.source_node, curr_time, size, exclusions, priority
        )
        self.cache.store(CacheEntry(self.routes, size, exclusions))

        unreachable_count = 0
        for dest in destinations:
            if dest not in self.routes:
                if dest not in self.size_max:
                    self.size_max[dest] = size
                else:
                    if self.size_max[dest] > size:
                        self.size_max[dest] = size
                unreachable_count += 1

        if unreachable_count == len(destinations):
            return {}, None, None

        # Because the extension block comes into play for multicast,
        # and the tree construction is not aware of the EB overhead (yet)

        success, booking, bundles_to_reschedule = self._schedule_multicast(
            curr_time,
            destinations,
            size,
            expiration,
            eb_cost_func,
            len(destinations) != 1,
            priority,
            bundle_ref,
        )
        if success:
            return self._first_hop_contact(destinations), booking, bundles_to_reschedule

        return {}, None, None

    def _schedule_multicast(
        self,
        curr_time: float,
        destinations: List[Union[str, int]],
        size: int,
        expiration,
        eb_cost_func,
        dry_run,
        priority,
        bundle_ref,
    ):  # pylint: disable=too-many-arguments, too-many-statements, too-many-locals
        """Wrap the dry_run and the scheduling (interval modifications).

        In some cases, the dry_run is unecessary, due to the capacity oriented search,
        and can be disabled with the dry_run boolean.
        eb_cost_func is a first attempt to dynamically calculate the size, if the EB is
        expected to change from hop to hop.

        This function is design as a generic solution for unicast and multicast, supply
        list of a single element for unicast.
        """

        def _try_routes(  # pylint: disable=too-many-locals,
            curr_time,
            reachable_destinations,
            size,
            route,
            expiration,
            is_source,
            eb_cost_func,
            acc,
            priority,
        ):
            if not is_source:
                contact = route.via_contact
                bdl_volume = size + eb_cost_func(reachable_destinations)
                # curr_time, bdl_volume, bdl_priority

                res = contact.manager.dry_run(curr_time, bdl_volume, priority, route)

                if not res:
                    return

                arrival_time = route.tx_end + contact.owlt

                if arrival_time > expiration:
                    return

                route.arrival_time = arrival_time
                curr_time = arrival_time

            next_routes = {}
            for dest in reachable_destinations:
                if route.to_node == dest:
                    acc.append(dest)
                else:
                    next_rte = route.children[dest]
                    if next_rte not in next_routes:
                        next_routes[next_rte] = [dest]
                    else:
                        next_routes[next_rte].append(dest)

            for next_rte, dests in next_routes.items():
                _try_routes(
                    curr_time,
                    dests,
                    size,
                    next_rte,
                    expiration,
                    False,
                    eb_cost_func,
                    acc,
                    priority,
                )

        def _update_routes(  # pylint: disable=too-many-arguments,too-many-locals
            curr_time,
            reachable_destinations,
            size,
            route,
            eb_cost_func,
            is_source,
            bundle_booking,
            priority,
            bundle_ref,
            bundles_to_reschedule,
        ):

            if not is_source:
                contact = route.via_contact
                bdl_volume = size + eb_cost_func(reachable_destinations)
                schedule_output = contact.manager.schedule(
                    bdl_volume, priority, bundle_ref, route
                )
                bundle_booking.append(
                    (
                        schedule_output.tx_start,
                        bdl_volume,
                        contact.manager,
                    )
                )
                bundles_to_reschedule.extend(schedule_output.bundles_to_reassign)

            next_routes = {}
            for dest in reachable_destinations:
                if route.to_node == dest:
                    continue
                next_rte = route.children[dest]
                if next_rte not in next_routes:
                    next_routes[next_rte] = [dest]
                else:
                    next_routes[next_rte].append(dest)

            for next_rte, dests in next_routes.items():
                _update_routes(
                    curr_time,
                    dests,
                    size,
                    next_rte,
                    eb_cost_func,
                    False,
                    bundle_booking,
                    priority,
                    bundle_ref,
                    bundles_to_reschedule,
                )

        # pre-filtering
        first_filtering = []

        for dest in destinations:
            if dest not in self.routes:
                continue
            if not self.routes[dest].parenting_set:
                self._set_parenting(dest)
            first_filtering.append(dest)

        source_route = self.routes[self.source_node]
        reachable_destinations = []
        if dry_run:
            # fills the reachable_destinations
            _try_routes(
                curr_time,
                first_filtering,
                size,
                source_route,
                expiration,
                True,
                eb_cost_func,
                reachable_destinations,
                priority,
            )
        else:
            # We should control expiration
            # Because dry_run is false, the tree is fresh,
            # i.e. the arrival time for the route
            # is the arrival time for the bundle
            for dest in first_filtering:
                if self.routes[dest].arrival_time <= expiration:
                    reachable_destinations.append(dest)

        if len(reachable_destinations) == 0:
            return False, None, None

        bundle_booking = []
        bundles_to_reschedule = []
        _update_routes(
            curr_time,
            reachable_destinations,
            size,
            source_route,
            eb_cost_func,
            True,
            bundle_booking,
            priority,
            bundle_ref,
            bundles_to_reschedule,
        )

        return True, bundle_booking, bundles_to_reschedule

    def _set_parenting(self, destination: List[Union[str, int]]):
        """
        Set the parenting for the current destination.

        Indeed SPSN only finds reverse paths. We reconstruct the path just before
        routing, to avoid unnecessary route construction.
        """
        route = self.routes[destination]
        curr_route = route

        if curr_route.children is None:
            curr_route.children = {}
        while curr_route.parent is not None:
            if curr_route.parent.children is None:
                curr_route.parent.children = {}
            curr_route.parent.children[route.to_node] = curr_route
            curr_route = curr_route.parent

        route.parenting_set = True

    def _first_hop_contact(self, destinations: Union[str, int]) -> Contact:
        """Return the first hops contacts dict for the destinations."""
        first_hops = {}
        for dest, rte in self.routes[self.source_node].children.items():
            if dest in destinations:
                if rte.via_contact in first_hops:
                    first_hops[rte.via_contact].append(dest)
                else:
                    first_hops[rte.via_contact] = [dest]

        return first_hops

    def __repr__(self):
        """
        Return a string representing a router.

        Returns
        -------
        str
            A string representing this router.
        """
        res = []
        res.append("Source: " + str(self.source_node))
        res.append("")
        for _, route in self.routes.items():
            res.append(str(route))

        return "\n".join(res)
