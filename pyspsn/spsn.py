"""Module for spsn."""
import sys
import copy as cpy
from typing import List, Dict, Union
from pyspsn.selfpriorityqueue import SelfPriorityQueue
from pyspsn.route import Route
from pyspsn.contact import Contact


def _erased_passed_contact(curr_time: float, priority_queue: SelfPriorityQueue):
    """
    Remove from the priority_queue the already ended contacts.

    Parameters
    ----------
    curr_time: float
        The current time.
    priority_queue: SelfPriorityQueue
        The contact list to prune.
    """
    while not priority_queue.empty() and curr_time > priority_queue.first.end:
        priority_queue.pop()


def _generate_hop_proposition(  # pylint: disable=too-many-arguments, too-many-locals
    curr_time: float,
    recv_contacts: SelfPriorityQueue,
    size: int,
    min_confidence: float,
    priority,
    dry_run_1,
    dry_run_2,
):
    """
    Return a Hop proposition carrying the information for the route search.

    Parameters
    ----------
    curr_time: float
        The current time.
    recv_contacts: SelfPriorityQueue
        The list of contacts for the search.
    size: int
        The volume that should pass through.
    min_confidence: float
        The minimum confidence allowed for the contacts.

    Returns
    -------
    Optional[HopProposition]
        The hop proposition that fullfil the requirements, None otherwise

    """
    # The following commented code supported simultaneous contacts between 2 nodes.
    # Rendered obsolete by the SABR specification of a contact plan.

    final_contact = None
    final_arrival_time = sys.maxsize
    curr_contact = recv_contacts.first

    # We use the dry_run for the first hit, then the tmp
    # if the tmp become
    found = 0
    # The dry_run_tmp object permits to reduce memory pressure
    last_dry_run = dry_run_1
    other = dry_run_2
    target = dry_run_1

    while curr_contact is not None:
        if curr_contact.confi >= min_confidence:
            # No contact will be better
            if final_contact is not None and curr_contact.start > final_arrival_time:
                # Here is the essence of the algorithm
                break

            # Dry run can be data destructive on the target object
            res = curr_contact.manager.dry_run(curr_time, size, priority, target)

            if res:
                arrival_time = target.tx_end + curr_contact.owlt
                # Keep the contact if better
                if final_contact is None or arrival_time < final_arrival_time:
                    last_dry_run = target
                    found += 1
                    target = other
                    other = last_dry_run

                    final_contact = curr_contact
                    final_arrival_time = arrival_time

        curr_contact = curr_contact.next

    if found > 1:
        return final_contact, final_arrival_time, last_dry_run

    return final_contact, final_arrival_time, dry_run_1


def _update_metrics(
    sndr_route: Route, recv_route: Route, hop_proposition, arrival_time
):
    """
    Update the metrics of the recv_route with the hop proposition.

    Parameters
    ----------
    source_route: Route
        The source route.
    sndr_route: Route
        The route of the sender.
    recv_route: Route
        The route to update, the receiver route.
    hop_proposition: HopProposition
        The hop proposition for the updates.

    """
    # arrival time
    recv_route.arrival_time = arrival_time

    # hop
    recv_route.via_contact = hop_proposition

    # sum owlt
    recv_route.sumowlt = sndr_route.sumowlt + recv_route.via_contact.owlt

    # parent
    recv_route.parent = sndr_route

    # number of hops
    recv_route.hop_count = sndr_route.hop_count + 1

    # confidence
    recv_route.confidence = sndr_route.confidence * hop_proposition.confi

    # expiration stream
    recv_route.expiration = min(
        recv_route.seg_end - sndr_route.sumowlt,
        sndr_route.expiration,
    )


def _insert_and_prune(idx, acc, ordered_routes, route_proposition):
    copy = cpy.copy(route_proposition)
    next_route = ordered_routes[idx]
    ordered_routes.insert(idx, copy)
    acc.update(copy)

    index = idx + 1

    while index < len(ordered_routes):
        next_route = ordered_routes[index]
        cond1 = copy.arrival_time <= next_route.arrival_time
        cond2 = copy.hop_count <= next_route.hop_count
        # cond3 = copy.expiration >= next_route.expiration

        if cond1 and cond2:  # and cond3:
            rte = ordered_routes.pop(idx + 1)
            acc.erase(rte)
        else:
            index += 1

    if idx == 0:
        return copy

    return None


def _mpt_helper(
    acc, ordered_routes, route_proposition
):  # pylint: disable=too-many-branches

    if len(ordered_routes) == 0:
        copy = cpy.copy(route_proposition)
        ordered_routes.append(copy)
        acc.update(copy)
        return copy

    insert = False
    idx = -1  # Make the linter happy
    # find the right place for this route
    for idx, rte in enumerate(ordered_routes):

        # Better delivery time
        if route_proposition.arrival_time < rte.arrival_time:
            insert = True
            break
        if route_proposition.arrival_time == rte.arrival_time:
            # Better hop count
            if route_proposition.hop_count < rte.hop_count:
                insert = True
                break
            if route_proposition.hop_count == rte.hop_count:
                # Better expiration time
                if route_proposition.expiration > rte.expiration:
                    insert = True
                    break
        else:  # route_proposition.arrival_time > rte.arrival_time
            if route_proposition.hop_count > rte.hop_count:
                insert = False
                break
            if route_proposition.hop_count == rte.hop_count:
                if route_proposition.expiration <= rte.expiration:
                    insert = False
                    break
            else:  # route_proposition.hop_count < rte.hop_count:
                insert = True

    if insert:
        return _insert_and_prune(idx, acc, ordered_routes, route_proposition)
    return None


class Spsn:
    """
    Core class holding the Dijkstra's algorithm implementation.

    Attributes
    ----------
    multigraph: Dict[Union[str, int], Dict[Union[str, int], SelfPriorityQueue]
        The time varying graph of the network.
    """

    __slots__ = ["managed", "multigraph", "tracking_method"]

    def __init__(
        self,
        contacts: List[Contact],
        managed=True,
        mpt_method=1,
    ):
        """
        Initialize the multigraph.

        Parameters
        ----------
        contacts: List[Contact]
            The list of contact to use to construct the multigraph.
        """
        self.managed = managed
        self.multigraph: Dict[
            Union[str, int], Dict[Union[str, int], SelfPriorityQueue]
        ] = {}

        self.tracking_method = mpt_method

        for contact in contacts:
            self._add_contact(contact)

    def _add_contact(self, contact: Contact):
        """
        Add a single contact to the multigraph.

        Parameters
        ----------
        contact: Contact
            The contact to add in the multigraph.
        """
        sndr = contact.sndr
        recv = contact.recv

        # if this sndr is unknown
        if sndr not in self.multigraph:
            self.multigraph[sndr] = {}

        # if this recv is unknown from this sndr
        if recv not in self.multigraph[sndr]:
            self.multigraph[sndr][recv] = SelfPriorityQueue()

        self.multigraph[sndr][recv].insert(contact)

    def update(self, contacts: List[Contact]):
        """
        Re-initialize the multigraph.

        Parameters
        ----------
        contacts: List[Contact]
            The list of contact to use to construct the multigraph.
        """
        self.multigraph = {}

        for contact in contacts:
            self._add_contact(contact)

    def compute(
        # pylint: disable=too-many-arguments,too-many-locals
        # pylint: disable=too-many-branches,too-many-statements
        self,
        source_node: Union[str, int],
        curr_time: float,
        size: int,
        exclusions,
        min_confidence: float = 0,
        priority=0,
    ) -> Dict[Union[str, int], Route]:
        """
        Dijstra Implementation.

        Parameters
        ----------
        source_node: Union[str, int]
            The source node for the search.
        curr_time: float
            The current time for the search.
        size: int
            The minimum volume that the routes shall offer.
        min_confidence: float
            The minimum confidence allowed for the contacts.

        Returns
        -------
        Dict[Union[str, int], Route]
            For each reachable node, the route to this node.
        """
        best_routes = {}
        routes = {}

        source_route = Route(source_node)
        source_route.arrival_time = curr_time
        best_routes[source_node] = source_route
        routes[source_node] = [source_route]

        route_proposition = None
        dry_run_1 = Route(-1)
        dry_run_2 = Route(-1)
        acc = SelfPriorityQueue()
        acc.insert(source_route)

        while not acc.empty():

            sndr_route = acc.pop()

            recvs_to_remove = []

            if sndr_route.to_node not in self.multigraph:
                continue

            for recv_node, recv_contacts in self.multigraph[sndr_route.to_node].items():

                if recv_node in exclusions:
                    continue

                # Manage the multigraph
                if self.managed:
                    _erased_passed_contact(curr_time, recv_contacts)

                if recv_contacts.empty():
                    recvs_to_remove.append(recv_node)
                    continue

                # generate a hop proposition
                (
                    hop_proposition,
                    arrival_time,
                    route_proposition,
                ) = _generate_hop_proposition(
                    sndr_route.arrival_time,
                    recv_contacts,
                    size,
                    min_confidence,
                    priority,
                    dry_run_1,
                    dry_run_2,
                )
                if hop_proposition is None:
                    continue

                route_proposition.to_node = recv_node

                # Node based parenting
                if self.tracking_method == 0:

                    _update_metrics(
                        sndr_route,
                        route_proposition,
                        hop_proposition,
                        arrival_time,
                    )

                    if recv_node not in best_routes:
                        copy = cpy.copy(route_proposition)
                        best_routes[recv_node] = copy
                        acc.update(copy)
                        continue

                    recv_route = best_routes[recv_node]

                    if route_proposition < recv_route:
                        route_proposition.copy_to_target(recv_route)
                        acc.update(recv_route)
                        continue

                # Node based parenting, trust first arrival time encountered, update
                elif self.tracking_method == 1:

                    _update_metrics(
                        sndr_route,
                        route_proposition,
                        hop_proposition,
                        arrival_time,
                    )

                    if recv_node not in best_routes:
                        copy = cpy.copy(route_proposition)
                        best_routes[recv_node] = copy
                        acc.update(copy)
                        continue

                    recv_route = best_routes[recv_node]
                    if route_proposition.hop_count < recv_route.hop_count or (
                        route_proposition.hop_count == recv_route.hop_count
                        and route_proposition < recv_route
                    ):
                        route_proposition.copy_to_target(recv_route)
                        acc.update(recv_route)
                        continue

                # SABR like tracking
                elif self.tracking_method == 2:

                    if recv_node not in routes:
                        routes[recv_node] = []

                    _update_metrics(
                        sndr_route,
                        route_proposition,
                        hop_proposition,
                        arrival_time,
                    )

                    inserted_route = _mpt_helper(
                        acc, routes[recv_node], route_proposition
                    )
                    if inserted_route is not None:
                        best_routes[recv_node] = inserted_route

                else:
                    raise ValueError("Unknown mpt_method")

            # Manage multigraph
            for receiver_node in recvs_to_remove:
                del self.multigraph[sndr_route.to_node][receiver_node]
            if len(self.multigraph[sndr_route.to_node]) == 0:
                del self.multigraph[sndr_route.to_node]

        return best_routes
