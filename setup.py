"""Make pyspsn pip installable."""

from setuptools import setup, find_packages


install_requires = []

setup(
    name="pyspsn",
    version="1.0.0",
    python_requires=">=3.6",
    packages=find_packages(),
    install_requires=install_requires,
)
