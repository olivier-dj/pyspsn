# SPSN (Shortest-Path tree routing for Space Networks)

Research implementation for SPSN, this is a WIP project.

### Functioning

SPSN is an alternative to Contact Graph Routing (CGR) for scheduled space delay-tolerant networks. SPSN uses like CGR an underlying Dijkstra's shortest path algorithm for pathfinding but differs in its implementation, its alternative route search strategy, and its route management strategy.

#### Shortest-path tree search and node graph design

The underlying Dijkstra algorithm is implemented with a node multigraph (in opposition to the contact graph approach). The contacts are sorted by start time between a sender and a receiver, and the expired contacts are pruned directly during the search to maintain a controlled computing time.

The shortest-path tree approach (shortest-path tree) permits to compute at once one path for each other node in the network. This decreases on a macro scale the computational time by preparing routes for the upcoming messages having different destinations.


#### Capacity oriented search and shadow level

SPSN computes shortest-path trees on which it relies for the routing decisions. The shortest-path tree is also computed with a volume as a parameter, which constrains the Dijkstra implementation to not include paths which don't show sufficient residual capacities. In other words, the Dijkstra implementation will always find the shortest valid path for a message of a given size.


When a shortest-path tree is computed for a given requested volume, the shortest-path tree remains valid for the next messages that have to be routed, only if those next messages have a size greater or equal than this volume, referred to as the "shadow level" of the shortest-path tree.

Indeed paths having a better delivery time but with small residual capacities might be "shadowed" if the shortest-path tree has been computed with a bigger requested volume. If a message having a size smaller to the current shadow level has to be routed, the shortest-path tree is recomputed or another tree is utilized from the cache, and the size of this message becomes the new shadow level of the shortest-path tree. The same way if the current shortest-path tree shows a capacity shortage for a destination, and a message having a greater size than the current shadow level has to be routed for this destination, the shortest-path tree will be recomputed and the shadow level updated accordingly.

Every route checking is directly operated on the shortest-path tree, this functioning obsoletes the need of routing tables.

#### Contact segmentation

Instead of representing the capacity of the contact with a single value, SPSN implements contact segmentation, by allowing contacts to have several intervals of available bandwidth. For example, when a message is scheduled for a new contact, if the transmission is planned for the beginning of this contact, the start time of its single partition becomes its original start time plus the transmission time of the message. If the message is not scheduled for the very beginning of the contact but later during the available partition, the partition will have to be split into two new partitions, the start time of the second minus the end time of the first being the transmission time of the message.

This functioning permits to limit faulty routing decisions and obsoletes the concept of volume. This can be tested directly on the "envelope network", a small network topology which can be used to test the ability of a routing algorithm to care of the bandwidth utilization to avoid faulty routing decisions.

#### Caching

The caching mechanism works by attempting to reuse trees of same exclusion lists and compatible shadow level for a bundle. The cache stores tress with the exclusion list as primary metric (tuple as key in a dictionary), but still filters by shadow level before returning any cache entry.


### Installation
```
pip install .
```
### Quick test

On a random contact plan:
```
python3 tests/test_random.py <contats> <nodes>
```

On the envelope network:
![](images/envelope_network.png)
```
python3 tests/test_envelope_network.py
```

### Contact

Reach me at <olivierdejonckere@gmail.com>.

### Links

* [pyCGR](https://bitbucket.org/juanfraire/pycgr)
